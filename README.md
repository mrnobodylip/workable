Shchapov Egor
========================

Hi there. Provided link to NASA documentation is broken, or I don't understand how to log in :)
So i've used info from here https://api.nasa.gov/api.html#neows-feed

I understand that it's better to use FOSRestBundle for creating an API,
but it's just an interview and there are a lot of thing to make better, but it's about time :)

So I'm not sure about two things:

* Data from NASA about asteroid speed is could change with close_approach_data change, so maybe should i move it to another table and calculate it from there, but i'm not sure.
* Second one is about last task, and phrase "not a month in a year". I really don't understand what does it means. So i used calculation just for every pair year+month and so.

And first task is on /update url. And don't forget to apply migrations with `php bin/console doctrine:migrations:migrate`