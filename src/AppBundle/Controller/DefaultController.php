<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Neo;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use GuzzleHttp\Client;

class DefaultController extends Controller
{
    /** api_key for NASA api */
    const API_KEY = 'N7LkblDsc5aen05FJqBQ8wU4qSdmsftwJagVK7UD';

    /**
     * @Route("/", name="homepage")
     * @return JsonResponse
     */
    public function indexAction()
    {
        return new JsonResponse(['hello' => 'world']);
    }

    /**
     * Get data for last 3 days from NASA and save it
     *
     * @Route("/update")
     * @return JsonResponse
     */
    public function updateAction()
    {
        $dateEnd = (new \DateTime())->setTime(0,0,0);
        $dateStart = (clone $dateEnd)->add(\DateInterval::createFromDateString('-2 days'));

        $client = new Client();
        $response = $client->get('https://api.nasa.gov/neo/rest/v1/feed', [
            'query' => [
                'start_date' => $dateStart->format('Y-m-d'),
                'end_date'   => $dateEnd->format('Y-m-d'),
                'api_key'    => self::API_KEY,
            ]
        ]);

        if ($response->getStatusCode() === 200) {
            $body = \GuzzleHttp\json_decode($response->getBody(), true);

            $em = $this->getDoctrine()->getManager();
            $rep = $em->getRepository(Neo::class);
            $validator = $this->get('validator');

            foreach ($body['near_earth_objects'] as $date => $items) {
                foreach ($items as $item) {

                    // check if it's already in db or not
                    $neo = $rep->findOneBy([
                        'date' => \DateTime::createFromFormat('Y-m-d', $date)->setTimezone(new \DateTimeZone('UTC'))->setTime(0,0,0)->format('U'),
                        'reference' => $item['neo_reference_id']
                    ]);

                    if (is_null($neo)) {
                        $neo = new Neo();
                    }

                    $neo->setName($item['name'])
                        ->setIsHazardous($item['is_potentially_hazardous_asteroid'])
                        ->setSpeed($item['close_approach_data'][0]['relative_velocity']['kilometers_per_hour'])
                        ->setReference($item['neo_reference_id'])
                        ->setDate($date)
                    ;

                    $errors = $validator->validate($neo);
                    if (empty($neo->getId()) && count($errors) > 0) {
                        $em->persist($neo);
                    }
                }

                $em->flush();
            }

            return new JsonResponse($body['element_count']);
        }

        return new JsonResponse('smth bad happened');
    }
}
