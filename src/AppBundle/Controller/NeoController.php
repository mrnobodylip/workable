<?php
/**
 * Date: 08.10.2017 21:00
 * @copyright Copyright (c) 2017 Egor Shchapov
 */

namespace AppBundle\Controller;

use AppBundle\Entity\Neo;
use Doctrine\ORM\Query;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Cache\Simple\FilesystemCache;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class NeoController extends Controller
{
    /**
     * @Route("/neo/hazardous", name="hazardous")
     *
     * @return Response
     */
    public function hazardousAction()
    {
        $items = $this->getDoctrine()
            ->getRepository(Neo::class)
            ->findBy([
                'isHazardous' => true,
            ])
        ;

        $response = new Response($this->getSerializer()->serialize($items, 'json'));
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
     * @Route("/neo/fastest", name="fastest")
     * @param Request $request
     *
     * @return Response
     */
    public function fastestAction(Request $request)
    {
        $hazardous = $request->query->get('hazardous', false);
        if (!is_bool($hazardous)) {
            $hazardous = false;
        }

        $cacheKey = 'fastest' . (int) $hazardous;
        $cache = new FilesystemCache('neo', 60 * 60);

        // fetch the item from the cache
        if (!$cache->has($cacheKey)) {
            $item = $this->getDoctrine()
                ->getRepository(Neo::class)
                ->findOneBy([
                    'isHazardous' => $hazardous,
                    'speed'       => 'DESC'
                ])
            ;
        } else {
            $item = $cache->get($cacheKey);
        }

        $response = new Response($this->getSerializer()->serialize($item, 'json'));
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
     * @Route("/neo/best-year", name="best-year")
     * @param Request $request
     *
     * @return Response
     */
    public function bestYearAction(Request $request)
    {
        $hazardous = $request->query->get('hazardous', false);
        if (!is_bool($hazardous)) {
            $hazardous = false;
        }

        $cacheKey = 'best_year' . (int) $hazardous;
        $cache = new FilesystemCache('neo', 60 * 60);

        // fetch the item from the cache
        if (!$cache->has($cacheKey)) {

            /** @var \Doctrine\ORM\QueryBuilder $em */
            $em = $this->getDoctrine()->getManager()->createQueryBuilder();

            // SELECT YEAR(FROM_UNIXTIME(`date`)) as `year`, COUNT(*) as `cnt` FROM `neo` GROUP BY `year` ORDER BY `cnt` DESC;
            $query = $em
                ->select('YEAR(FROM_UNIXTIME(n.date)) AS best_year', 'n.id')
                ->addSelect('COUNT(n.id) AS cnt')
                ->from('AppBundle:Neo', 'n')
                ->where('n.isHazardous = :is_hazardous')
                ->setParameter('is_hazardous', $hazardous)
                ->groupBy('best_year')
                ->orderBy('cnt', 'DESC')
                ->setMaxResults(1)
                ->getQuery();

            $item = $query->getResult(Query::HYDRATE_ARRAY);
        } else {
            $item = $cache->get($cacheKey);
        }

        $response = new Response($this->getSerializer()->serialize($item, 'json'));
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }


    /**
     * @Route("/neo/best-month", name="best-month")
     * @param Request $request
     *
     * @return Response
     */
    public function bestMonthAction(Request $request)
    {
        $hazardous = $request->query->get('hazardous', false);
        if (!is_bool($hazardous)) {
            $hazardous = false;
        }

        $cacheKey = 'best_month' . (int) $hazardous;
        $cache = new FilesystemCache('neo', 60 * 60);

        // fetch the item from the cache
        if (!$cache->has($cacheKey)) {
            /** @var \Doctrine\ORM\QueryBuilder $em */
            $em = $this->getDoctrine()->getManager()->createQueryBuilder();

            // SELECT CONCAT(MONTH(FROM_UNIXTIME(`date`)), '_', YEAR(FROM_UNIXTIME(`date`))) as `month_year`, COUNT(*) as `cnt` FROM `neo` WHERE is_hazardous = 0 GROUP BY `month_year` ORDER BY `cnt` DESC;
            $query = $em
                ->select('CONCAT(MONTH(FROM_UNIXTIME(n.date)), \'_\', YEAR(FROM_UNIXTIME(n.date))) AS best_month', 'n.id')
                ->addSelect('COUNT(n.id) AS cnt')
                ->from('AppBundle:Neo', 'n')
                ->where('n.isHazardous = :is_hazardous')
                ->setParameter('is_hazardous', $hazardous)
                ->groupBy('best_month')
                ->orderBy('cnt', 'DESC')
                ->setMaxResults(1)
                ->getQuery();

            $item = $query->getResult(Query::HYDRATE_ARRAY);
            $cache->set($cacheKey, $item);
        } else {
            $item = $cache->get($cacheKey);
        }

        $response = new Response($this->getSerializer()->serialize($item, 'json'));
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
     * @return Serializer
     */
    private function getSerializer()
    {
        $encoders = array(new XmlEncoder(), new JsonEncoder());
        $normalizers = array(new ObjectNormalizer());

        return new Serializer($normalizers, $encoders);
    }
}