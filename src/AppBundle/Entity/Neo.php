<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Neo
 *
 * @ORM\Table(
 *     name="neo",
 *     indexes={
 *      @ORM\Index(name="speed_idx", columns={"speed"}),
 *      @ORM\Index(name="date_idx", columns={"date"}),
 *      @ORM\Index(name="is_hazardous_idx", columns={"is_hazardous"}),
 *     }
 * )
 * @ORM\Entity(repositoryClass="AppBundle\Repository\NeoRepository")
 */
class Neo
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @Assert\NotBlank()
     * @Assert\Type(
     *     type="integer",
     * )
     * @ORM\Column(name="date", type="integer")
     */
    private $date;

    /**
     * @var int
     *
     * @Assert\NotBlank()
     * @Assert\Type(
     *     type="integer",
     * )
     * @ORM\Column(name="reference", type="integer")
     */
    private $reference;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Type(
     *     type="string",
     * )
     * @Assert\Length(
     *     max=255
     * )
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var float
     *
     * @Assert\NotBlank()
     * @Assert\Type(
     *     type="float",
     * )
     * @ORM\Column(name="speed", type="float")
     */
    private $speed;

    /**
     * @var bool
     *
     * @Assert\NotBlank()
     * @Assert\Type(
     *     type="bool",
     * )
     * @ORM\Column(name="is_hazardous", type="boolean")
     */
    private $isHazardous;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param string $date
     *
     * @return Neo
     */
    public function setDate($date)
    {
        $date = \DateTime::createFromFormat('Y-m-d', $date)
            ->setTimezone(new \DateTimeZone('UTC'))
            ->setTime(0,0,0)
        ;

        $this->date = $date->format('U');

        return $this;
    }

    /**
     * Get date
     *
     * @return string
     */
    public function getDate()
    {
        $date = \DateTime::createFromFormat('U', $this->date)
            ->setTimezone(new \DateTimeZone('UTC'))
        ;

        return $date->format('Y-m-d');
    }

    /**
     * Set reference
     *
     * @param integer $reference
     *
     * @return Neo
     */
    public function setReference($reference)
    {
        $this->reference = $reference;

        return $this;
    }

    /**
     * Get reference
     *
     * @return int
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Neo
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set speed
     *
     * @param integer $speed
     *
     * @return Neo
     */
    public function setSpeed($speed)
    {
        $this->speed = $speed;

        return $this;
    }

    /**
     * Get speed
     *
     * @return int
     */
    public function getSpeed()
    {
        return $this->speed;
    }

    /**
     * Set isHazardous
     *
     * @param boolean $isHazardous
     *
     * @return Neo
     */
    public function setIsHazardous($isHazardous)
    {
        $this->isHazardous = $isHazardous;

        return $this;
    }

    /**
     * Get isHazardous
     *
     * @return bool
     */
    public function getIsHazardous()
    {
        return $this->isHazardous;
    }
}

